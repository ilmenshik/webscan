function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}

function setQualityValue(value) {
	if (value) {
		$(".scanImageQuality").val(value);
	}
	$('.scanImageQualityValue').text($(".scanImageQuality").val());
}

function getArchiveName() {
    var d = new Date();
    return d.getFullYear() + "" +
           ("0" + (d.getMonth()+1)).slice(-2) + "" +
           ("0" + d.getDate()).slice(-2) + "-" +
           ("0" + d.getHours()).slice(-2) + "" +
           ("0" + d.getMinutes()).slice(-2) + "" +
           ("0" + d.getSeconds()).slice(-2) + "-" +
           getRandomInt(100,999);
}

function delay(ms) {
	var cur_d = new Date();
	var cur_ticks = cur_d.getTime();
	var ms_passed = 0;
	while(ms_passed < ms) {
		var d = new Date();  // Possible memory leak?
		var ticks = d.getTime();
		ms_passed = ticks - cur_ticks;
		// d = null;  // Prevent memory leak?
	}
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function scanRemove(){
	var o = $(this)
	$.post( "/api/delete", {'files': [o.attr('data')]}, function( data ) {
	    console.log([o.attr('data')]);
		console.log(data);
		o.parent().parent().fadeOut( "slow", function() {
            count = $(this).parent().parent().parent().parent().find('tr').length - 1
            $(this).remove();
            if (count == 1) {
                $('.scanShowAll').hide();
                $('.scanEmpty').show();
            }
        });
	});
	return false;
}

function archiveRecommend() {
	if ($('.scanExtension').val() == 'jpg' && $('.scanSource').val() == 'ADF' && $('.scanPages').val() == '99') {
		$('.scanArchiveRecommend').show();
		$('.scanArchive').val('zip');
	} else {
		$('.scanArchiveRecommend').hide();
	}
}

function isSomethingSelected() {
	return $('.scanMassActions:checked').length != 0
}

function isNotAllSelected() {
	return $('.scanMassActions').length != $('.scanMassActions:checked').length
}

function selectAllText() {
	if (isNotAllSelected()) {
		$('.scanMassActionSelectAll').show();
		$('.scanMassActionSelectNone').hide();
	} else {
		$('.scanMassActionSelectAll').hide();
		$('.scanMassActionSelectNone').show();
	}

	if (isSomethingSelected()) {
        $('.scanMassActionButton button').prop('disabled', false).removeClass('btn-outline-secondary');
	} else {
        $('.scanMassActionButton button').prop('disabled', true).addClass('btn-outline-secondary');
	}
}

function getFileExten(filename) {
    return filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename;
}

function getSelectedFiles() {
    files = []
    $('.scanMassActions:checked').each(function(){
        files.push( $(this).parent().parent().find('.scanRemove').attr('data') );
    });
    return files;
}

function show_error(text){
    bs4pop.alert(text, null, {'title': 'Ошибка', 'className': 'alert-dialog'});
}

function show_loader() {
		$('.scanError').hide();
		$('.scanSuccess').hide();
		$('.scanForm').hide();
		$('.scanResult').hide();
		$('.scanLoader').show();
        $('.scanDebugLog').text('');
}

//////////////////////////////////////////////////////////////////////


$(function(){
    $('.scanSubmitter').keydown(function (e) {
        if (e.keyCode == 13) {
            $('.scanStart').click();
        }
    });

	$('.scanStart').click(function(){
		show_loader()
		sendData = $( ".scanForm" ).serialize();
		$.post( "/api/scan", sendData, function( data ) {

			$('.scanLoader').hide();
            console.log('Received data:', data)
			try {
				if (data.success) {
					$('.scanError').hide();
					console.log(data.data)
					if (data.data) {
						$('.scanSuccessMessage').html('<a href="/download/'+data.data.result_name+'">Скачать результат ('+data.data.result_name+')</a>');
						$('.scanListTable > tbody > tr:first').after(data.data.row);
						obj = $('.scanListTable > tbody > tr').eq(1)
						obj.addClass('scanNew');
						obj.find('.scanRemove').click(scanRemove);
						setTimeout(function(){
							obj.removeClass('scanNew');
						}, 5000);
					} else {
						$('.scanSuccessMessage').html('<a href="/scans">Скачать результат</a>');
					}
					$('.scanSuccess').show();
					$('.scanShowAll').show();
					$('.scanEmpty').hide();
				} else {
					$('.scanErrorMessage').html(data.message);
					$('.scanError').show();
					$('.scanSuccess').hide();
                    $('.scanForm').show();
				}
			} catch(error) {
				console.error('ER:', error);
				$('.scanForm').show();
			}

		});
		return false;
	});

    $('.scanDeviceUpdate').click(function(){
		show_loader();

        $(this).hide();
        $.post('/api/devices',{}, function(data){
            console.log(data);
            location.reload();
        });
        return false;
    });

	$('.scanReset').click(function(){
		$('.scanLoader').hide();
		$('.scanError').hide();
		$('.scanSuccess').hide();
        number = $('.scanFilename').val().replace(/[^0-9]+([0-9]+)$/g, "$1")
        if (number && number.match(/^[0-9]+/)) {
            new_number = parseInt(number) + 1
            new_text = $('.scanFilename').val().replace(/([^0-9]+).*/g, "$1" + new_number?.toString())
        } else {
			num = Math.round(new Date().getTime() / 100 % 1000000);
			new_text = 'scan-' + num.toString()
        }
        $('.scanFilename').val(new_text);
		$('.scanForm').show();
		return false;
	});

	$('.scanExtension').change(function(){
		archiveRecommend();
	});

	$('.scanSource').change(function(){
		if ($(this).val() == 'ADF') {
			$('.scanPagesOnlyOne').hide();
			$('.scanPagesHolder').show();
			$('.scanPages').change();
		} else {
			$('.scanPagesOnlyOne').show();
			$('.scanPagesHolder').hide();
			$('.scanPagesCountHolder').hide();
		}
		archiveRecommend();
	});

	$('.scanPages').change(function(){
		if ($(this).val() == '99') {
			$('.scanPagesCountHolder').show();
			$('.scanPagesCount').val();
		} else {
			$('.scanPagesCountHolder').hide();
		}
		archiveRecommend();
	});

	$('.scanDPI').change(function(){
		if ($(this).val() > 300) {
			$('.scanWarningTime').show();
		} else {
			$('.scanWarningTime').hide();
		}
	});

	$(".scanImageQuality").on('input', function () {
		setQualityValue();
    });

	$('.scanRemove').click(scanRemove);


	//////////////////////////////////////////////////////////////////////


	$('.scanMassActionSelect').click(function(){
		$('.scanMassActions').prop('checked', isNotAllSelected());
		selectAllText();
	});

    $('.scanMassActionMerge').click(function(){
        files = getSelectedFiles();
		if (!files.length) {
            show_error('Не выбрано ни одного файла');
			return;
		}

        bs4pop.prompt('Введите имя конечного файла', 'scans-'+getArchiveName(), function(sure, result_name, format){
			if (!sure) return;
			if (!result_name) return;

            $('.interfaceBlocker').fadeIn('slow');
            $.post('/api/convert',{
                files: files,
                file_name: result_name,
                file_type: format
            }, function(data){
                try {
                    obj = jQuery.parseJSON( data );
                    if (obj.code == 0) {
                        location.reload();
                    } else {
                        show_error(obj.log);
                    }
                } catch (error) {
                    console.error(data);
                }
            });
            $('.interfaceBlocker').hide();
		}, {'title': 'Конвертирование', 'customExtensions': true});
	});

    $('.scanMassActionArchive').click(function(){
		files = getSelectedFiles();
		if (!files.length) {
			show_error('Не выбрано ни одного файла');
			return;
		}

		bs4pop.prompt('Введите имя архива', 'scans-'+getArchiveName(), function(sure, archive_name){
			if (!sure) return;
			if (!archive_name) return;

			console.log(files);
            $('.interfaceBlocker').fadeIn('slow');
			$.post('/api/archive',{
				files: files,
				file_name: archive_name
			}, function(data){
                console.log(data);
                location.reload();
			});
		}, {'title': 'Архивирование файлов'});
	});

    $('.scanMassActionDelete').click(function(){
        files = getSelectedFiles();
        if (!files.length) {
            show_error('Не выбрано ни одного файла');
            return;
        }
        bs4pop.confirm('Вы действительно хотите удалить '+files.length+' файл'+declOfNum(files.length, ['', 'а', 'ов']), function(sure, archive_name){
            if (!sure) return;
            $.post('/api/delete',{
				files: files,
			}, function(data){
                console.log(data);
                $('.scanMassActions:checked').each(function(){
                    $(this).parent().parent().fadeOut( "slow", function() {
                        $(this).remove();
                    });
                });
			});
        }, {'title': 'Удаление файлов'});
    });

	$('.scanMassActions').click(function(){
		selectAllText();
	})
	//////////////////////////////////////////////////////////////////////

	$('.scanExtension').change();
	$('.scanSource').change();
	$('.scanPages').change();
	$('.scanDPI').change();
	setQualityValue();

	if ($('.scanForm').attr('data') == '0') {
		$('.scanForm').find('fieldset').attr('disabled', 'disabled');
		$('.scanStart').addClass('btn-secondary');
		$('.scanDevice').addClass('scanDisabled');
	}
	$('.scanMassActions').prop('checked', false);
	selectAllText();
});
