from datetime import datetime
from os import listdir
from os.path import getmtime, basename, getsize, isdir, join, isfile, realpath, dirname
from typing import List
from re import compile

SCANNS_WORKDIR = dirname(realpath(__file__))
SCANNS_DIR = join(SCANNS_WORKDIR, 'scans')
SCANNS_ALLOWED_EXTEN = ['pdf', 'jpg', 'png', 'tif', 'zip', 'tgz']
SCANNS_INVALID_CHARS = compile(r'[^A-Za-z0-9А-Яа-яЁё ._-]')


class ScanFile:
    datetime = None
    name = None
    path = None
    size = None
    exten = None

    def __init__(self, file):
        self.created = getmtime(file.name)
        self.datetime = datetime.fromtimestamp(self.created).strftime('%Y-%m-%d %H:%M')
        self.path = file.name
        self.name = basename(file.name)
        self.size = "{} Mb".format(round(getsize(file.name) / (1024**2), 2) or 0.01)
        parts = file.name.split('.')
        self.exten = parts[-1] if parts else None


def get_scan_list(dirpath=SCANNS_DIR) -> List[ScanFile]:
    if not dirpath or not isdir(dirpath):
        return []

    scan_list = list()
    for filename in listdir(dirpath):
        file = get_scan_object(filename)
        if file:
            scan_list.append(file)

    return sorted(scan_list, key=lambda x: x.created, reverse=True)


def get_scan_object(filename) -> ScanFile or None:
    filename = join(SCANNS_DIR, basename(filename))
    if not isfile(filename):
        return
    scan_file = ScanFile(open(filename))
    if scan_file.exten not in SCANNS_ALLOWED_EXTEN:
        return
    
    return scan_file


def plurals(number, one, two, five):
    """
    Вычисление окончания слов в зависимости от количества: файл, файла, файлов

    :param number: Количество
    :param one: Один (файл)
    :param two: Два (файлА)
    :param five: Пять (файлОВ)
    :return:
    """

    if not number:
        return five

    number = abs(number)

    if number > 99:
        number = number % 100

    last = number % 10

    if last in [2, 3, 4] and (number < 10 or number > 20):
        return two
    if last == 1 and number != 11:
        return one

    return five


def filter_result_filename(filename):
    return SCANNS_INVALID_CHARS.sub('', filename)
