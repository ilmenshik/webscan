#!/bin/bash
# Crontab task: remove scanns older than X days

set -e
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
LIMIT_DAYS=${1:-180}
if [[ ! -d "$DIR" && "$DIR" == '/' ]]; then
    exit 1
fi

if [[ ! -d "$DIR/scans" ]]; then
  exit 0
fi

cd "$DIR"

find scans/temp/ \
    -maxdepth 1 \
    -mtime +2 \
    -type f \
    -exec rm -f {} \;

find scans/ \
    -maxdepth 1 \
    -type f \
    -mtime +$LIMIT_DAYS \
    \( -iname \*.jpg -o -iname \*.png -o -iname \*.tif -o -iname \*.pdf -o -iname \*.tgz -o -iname \*.zip \) \
    -exec rm -f {} \;
