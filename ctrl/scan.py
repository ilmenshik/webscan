import logging
from json import dumps, loads
from os.path import realpath, dirname, join, isfile
from os import listdir, rename
from pathlib import Path
from subprocess import Popen, PIPE
from re import compile
from packaging import version
import click
from datetime import datetime
from shutil import rmtree, copyfile
from typing import List

ALLOWED_SCAN_FORMATS = ['tif', 'pdf', 'jpg', 'png']
ALLOWED_BATCH_FORMATS = ['tif', 'pdf']
ALLOWED_ARCHIVE_FORMATS = ['zip', 'tar', '']


class Defaults:
    format = 'pdf'
    source = 'Flatbed'
    pages = 1
    quality = 80
    archive_format = ''
    color = False
    dpi = 200


class ScanDevice:
    device_id = ''
    name = ''

    def __init__(self, device_id=None, name=None):
        self.device_id = device_id
        self.name = name


class ScanDeviceList:
    devices: List[ScanDevice] = list()
    device_file = None

    def __init__(self, device_file):
        self.device_file = device_file
        self.load_device_list()

    def clear(self):
        self.devices = list()

    def append(self, device, name):
        self.devices.append(ScanDevice(device, name))

    def parse_device(self, device_line):
        device, name = device_line.split(' ', 1) if device_line else (None, 'Unknown device')
        if device:
            logging.debug('Parsed {} with name {}'.format(device, name))
            self.devices.append(ScanDevice(device, name))

    def load_device_list(self):
        if not isfile(self.device_file):
            return

        logging.info('Gettting a static device list')
        with open(self.device_file, 'r') as file:
            data = loads(file.read())
            for item in data:
                self.append(item.get('device'), item.get('name'))

    def save_device_list(self):
        try:
            f = open(self.device_file, 'w')
            try:
                f.write(dumps([{
                    'device': dev.device_id,
                    'name': dev.name,
                } for dev in self.devices]))
            finally:
                f.close()
        except IOError:
            logging.exception('Write error')

    def device_exist(self, device_id):
        return any(x for x in self.devices if x.device_id == device_id)


class ScanResult:
    exit_code = 0
    error_log = None
    result_files = list()

    def __init__(self, code, log, files):
        self.exit_code = code
        self.error_log = log
        self.result_files = files


#############################################################################
# CONFIG
#############################################################################

DIR = dirname(realpath(__file__))
SCANNER_LIST = join(DIR, 'scanners_list.dat')
SCAN_DIR = join(DIR, 'scans')
TEST_MODE = False
LOGGING_LEVEL = logging.INFO
REGEX_SHELL_SEMI = compile(r'[;\s]*$')
DEVICE_LIST = ScanDeviceList(SCANNER_LIST)
SCANIMAGE_ADF_FORMAT = None


#############################################################################
# CONSTANTS
#############################################################################

COLUMS_FORMAT = '{0:32} {1}'
LOGGING_FORMAT = '%(asctime)s %(name)s [%(levelname)s] %(message)s'

NEW_LINE_REGEX = compile(r'\n$')
EXTEN_REGEX = compile(r'\.[a-z]+$')


#############################################################################
# Prepares
#############################################################################

logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)


#############################################################################
# METHODS
#############################################################################

def exec_bash(command):
    """
    Execute bash command
    :param command: Command
    :return: exit_code, log_lines_list
    """
    logging.debug('shell: {}'.format(command))
    process = Popen(command, shell=True, executable='/bin/bash', stdout=PIPE)
    with process.stdout:
        result_exit_code = process.wait()
        result_log = process.stdout.read().decode(encoding='UTF-8')
        return result_exit_code, result_log.split('\n') if result_log else result_log


def now():
    """
    Get current date-time ISO format
    :return: String
    """
    return datetime.now().strftime('%Y-%m-%d-%H%M%S')


def zero_fill(text, zero_num, fill_with='0'):
    """
    Fill number with zeroes in left
    :param text: String
    :param zero_num: Need length
    :param fill_with: Filler
    :return: String
    """
    for i in range(0, zero_num - len(text)):
        text = "{}{}".format(fill_with, text)
    return text


def fix_scan_names(directory, before_numbers='out', after_numbers='.tif'):
    """
    Rename result files for correct order in file-mask
    out1.tif -> out001.tif

    :param directory: Working directory
    :param before_numbers: Text before number in filename
    :param after_numbers: Text after number in filename
    :return:
    """
    mask = compile(r'{}[0-9]+{}'.format(before_numbers, after_numbers))
    file_list = [f for f in listdir(directory) if isfile(join(directory, f)) and mask.match(f)]
    zeroes_count = len(str(len(file_list))) + 1  # failsafe

    for file in file_list:
        spits = file.split('.')
        file_num = zero_fill(spits[0].split(before_numbers)[1], zeroes_count)
        rename_to = '{pref}{num}{ext}'.format(pref=before_numbers, num=file_num, ext=after_numbers)
        if file != rename_to:
            rename(join(directory, file), join(directory, rename_to))


def prepare_scan_names(directory, work_files, replace='convert', replace_with='output'):
    """
    Mass rename (replace text in filename)

    :param directory: Working directory
    :param work_files: Working files
    :param replace: Replace what
    :param replace_with: Replace with
    :return: New files list
    """
    for idx, file in enumerate(work_files):
        rename_to = file.replace(replace, replace_with)
        rename(join(directory, file), join(directory, rename_to))
        work_files[idx] = rename_to
    return work_files


def batch_to_single(directory):
    """
    Merge files out*.tif into one result.tif

    :param directory: Working directory
    :return: Result directory
    """
    ec, shell_log = exec_bash('cd {dir}; tiffcp -c lzw out*.tif "result.tif"; rm -f out*.tif'.format(dir=directory))
    for line in shell_log:
        logging.debug(line)

    return join(directory, 'result.tif')


def filter_source(source):
    global SCANIMAGE_ADF_FORMAT
    if not SCANIMAGE_ADF_FORMAT:
        ec, cur_version = exec_bash('sudo scanimage --version | awk \'{print $NF}\'')
        if ec != 0 or not cur_version or not cur_version[0]:
            return source
        if version.parse(cur_version[0]) >= version.parse("1.0.24"):
            SCANIMAGE_ADF_FORMAT = 'Automatic Document Feeder'
        else:
            SCANIMAGE_ADF_FORMAT = 'ADF'

    return SCANIMAGE_ADF_FORMAT if source == 'ADF' else source


def exec_scanner(workdir, device, source=Defaults.source, pages=Defaults.pages, color=Defaults.color,
                 dpi=Defaults.dpi) -> ScanResult:
    """
    Scan

    :param workdir: Working directory
    :param device: Scanner device
    :param source: Flatbed|ADF
    :param pages: Page count (pages > 1, only if source=ADF)
    :param color: Color|Gray
    :param dpi: DPI
    :return: ScanResult
    """
    cmd = 'cd {dir}; sudo scanimage \
        --device-name={dev} \
        --source="{src}" \
        --mode={mode} \
        --format=tiff \
        --resolution={dpi} \
        {batch};'.format(
       dir=workdir,
       src=filter_source(source),
       dev=device,
       mode='Color' if color else 'Gray',
       dpi=dpi,
       batch='> out1.tif' if pages == 1 else '--batch --batch-count={}'.format(pages)
    )

    if TEST_MODE:
        logging.info("Rewrited command: {}".format(cmd))
        cmd = 'cd "{dir}"; for i in $(seq 1 {pages}); do cp -v "{root}/test.SCAN.tif" "./{limit}"; done; sleep 3'.format(
            dir=workdir,
            pages=pages,
            root=DIR,
            limit='out.tif' if pages == 1 else 'out$i.tif',
        )

    logging.info('Scan cmd: {}'.format(cmd))
    logging.info('Scaning ...')
    ec, shell_log = exec_bash(cmd)
    for line in shell_log:
        logging.info(line)

    if ec != 0:
        return ScanResult(ec, "\n".join(shell_log), None)

    logging.info('Scan finished. Afterworks.')
    fix_scan_names(workdir, 'out', '.tif')
    return ScanResult(0, None, [batch_to_single(workdir)])


def tiff2png(source_file, workdir, quality=Defaults.quality):
    """
    Convert TIFF into PNG

    :param source_file: Source file path
    :param workdir: Working directory
    :param quality: Quality of images convertion
    :return:
    """
    prefix = 'convert'
    ec, shell_log = exec_bash('cd {dir}; convert -quality {qua} "{inp}" "{prefix}.png"'.format(
        dir=workdir,
        qua=quality,
        inp=source_file,
        prefix=prefix
    ))
    for line in shell_log:
        logging.debug(line)

    fix_scan_names(workdir, '{}-'.format(prefix), '.png')


def tiff2jpg(source_file, workdir, quality=Defaults.quality):
    """
    Конвертирование TIFF в JPG

    :param source_file: Source file path
    :param workdir: Working directory
    :param quality: Quality of images convertion
    :return:
    """
    prefix = 'convert'
    ec, shell_log = exec_bash('cd {dir}; convert -quality {qua} "{inp}" "{prefix}.jpg"'.format(
        dir=workdir,
        qua=quality,
        inp=source_file,
        prefix=prefix
    ))

    for line in shell_log:
        logging.debug(line)

    fix_scan_names(workdir, '{}-'.format(prefix), '.jpg')


def tiff2pdf(source_file, output_file, quality=Defaults.quality):
    """
    Convert TIFF into PDF

    :param source_file: Source file path
    :param output_file: Destination file path
    :param quality: Image quality
    :return:
    """
    if not output_file or not source_file:
        logging.error('Output or source file not set')
        return
    ec, shell_log = exec_bash('tiff2pdf -j -q {qua} -t "{title}" -o "{out}" "{inp}"'.format(
        inp=source_file,
        out=output_file,
        qua=quality,
        title="Scan {}".format(now())
    ))
    for line in shell_log:
        logging.debug(line)

    logging.info('Fixing PDF colors (jpeg compression bug)')
    # ColorTransform: 0 or 1
    ec, shell_log = exec_bash("sed -i 's|/DecodeParms << /ColorTransform 0 >>||' '{}'".format(output_file))

    for line in shell_log:
        logging.debug(line)

    return [output_file]


def pdf2tiff(source_file, output_file, density=288):
    """
    Convert PDF into TIFF

    :param source_file: Source file path
    :param output_file: Destination file path
    :param density: Density
    :return:
    """
    if not output_file or not source_file:
        logging.error('Output or source file not set')
        return
    ec, shell_log = exec_bash('convert -density {den} {inp} {out}'.format(
        inp=source_file,
        out=output_file,
        den=density,
    ))

    for line in shell_log:
        logging.debug(line)

    return [output_file]


def convert_scan(source_tiff, workdir, exten=Defaults.format, quality=Defaults.quality, output_prefix='convert'):
    """
    Convert from tiff into other format

    :param source_tiff: Source tiff file
    :param workdir: Working directory
    :param exten: Extension
    :param quality: Image quality
    :param output_prefix: Output filename prefix
    :return: List of result files
    """
    if exten == 'tif':
        output = join(workdir, '{}.tif'.format(output_prefix))
        logging.info('Renaming TIFF: {}'.format(output))
        rename(source_tiff, output)
        return [output]
    elif exten == 'pdf':
        output = join(workdir, '{}.pdf'.join(output_prefix))
        logging.info('Converting to PDF: {}'.format(output))
        tiff2pdf(source_tiff, output, quality)
        return [output]
    elif exten == 'jpg':
        logging.info('Converting to JPEG')
        tiff2jpg(source_tiff, workdir, quality)
        return [f for f in listdir(workdir) if isfile(join(workdir, f)) and '.jpg' in f]
    elif exten == 'png':
        logging.info('Converting to PNG')
        tiff2png(source_tiff, workdir, quality)
        return [f for f in listdir(workdir) if isfile(join(workdir, f)) and '.png' in f]


def move_result_image(work_files, output_name, exten, workdir):
    """
    Move result into scans directory

    :param work_files: Files list
    :param output_name: Output filenames
    :param exten: Extension
    :param workdir: Working directory
    :return:
    """
    i = 0
    count_files = len(work_files)
    zeroes = len(str(count_files))
    work_files.sort()
    result = list()
    for f in work_files:
        i += 1
        target = '{dir}/{name}-{num}.{ext}'.format(
            dir=SCAN_DIR,
            name=output_name,
            num=zero_fill(str(i), zeroes),
            ext=exten
        )
        rename(join(workdir, f), target)
        result.append(target)
        logging.info('Moded file: {}'.format(target))

    return result


def archive_exten(arch_type):
    """
    Get acrhive extension by archive method
    :param arch_type: Method (zip or tar)
    :return: String
    """
    return 'zip' if arch_type == 'zip' else 'tgz'


def archive_scans(workdir, file_mask, archive_type='zip', result_name='result'):
    """
    Archivate files
    :param workdir: Workdir
    :param file_mask: Filemask
    :param archive_type: Archive format (zip/tar)
    :param result_name Result filename
    :return:
    """
    archive_cmd = 'tar -cvzf' if archive_type == 'tar' else 'zip'
    result_archive_name = '{}.{}'.format(result_name if result_name else 'result', archive_exten(archive_type))
    logging.info('Arhive files into: {}'.format(result_archive_name))
    cmd = 'cd {dir}; {arc_cmd} {output} {files_mask}'.format(
        dir=workdir,
        arc_cmd=archive_cmd,
        output=result_archive_name,
        files_mask=file_mask
    )
    ec, shell_log = exec_bash(cmd)
    for line in shell_log:
        logging.debug(line)

    return result_archive_name


def get_device_list(force_update=False) -> List[ScanDevice]:
    """
    Get device list
    :param force_update: Force update list from scanimage
    :return:
    """
    if force_update:
        cmd = 'sudo scanimage --formatted-device-list="%d %v %m %t%n"'
        if TEST_MODE:
            cmd = 'cat {}/test.DEVICES'.format(DIR)

        logging.debug('Updating device list: {} ({} mode)'.format(cmd, 'test' if TEST_MODE else 'normal'))
        ec, shell_log = exec_bash(cmd)
        if ec == 0 and shell_log:
            DEVICE_LIST.clear()
            for line in shell_log:
                DEVICE_LIST.parse_device(line)
            DEVICE_LIST.save_device_list()

    return DEVICE_LIST.devices


def scan_images(device_id, output_name, source, pages, archive, dpi, color, exten, quality) -> ScanResult:
    """
    Scan images
    :param device_id: Device ID
    :param output_name: Output file name
    :param source: ADF/Flatbed
    :param pages: Number of pages
    :param archive: Archive type: None, tar, zip
    :param dpi: DPI
    :param color: Color/Gray
    :param exten: Format (tif, pdf, png, jpg)
    :param quality: Quality (percents)
    :return: ScanResult
    """
    if not DEVICE_LIST.device_exist(device_id):
        logging.error("Device '{}' not found in {}".format(device_id, [x.device_id for x in DEVICE_LIST.devices]))
        return ScanResult(1, "Device not found", None)

    workdir = join(SCAN_DIR, 'temp', 'tmpscan-{}'.format(now()))
    Path(workdir).mkdir(parents=True, exist_ok=True)
    try:
        result = exec_scanner(workdir, device_id, source, pages, color, dpi)
        if result.exit_code != 0:
            return ScanResult(result.exit_code, result.error_log, None)

        work_files = convert_scan(result.result_files[0], workdir, exten, quality)
        if not work_files:
            logging.error('Empty work files array')
            return ScanResult(1, 'Empty work files array', None)

        work_files = prepare_scan_names(workdir, work_files, 'convert', output_name)

        if not archive and exten in ['jpg', 'png'] and pages > 1:
            work_files = move_result_image(work_files, output_name, exten, workdir)
            logging.info('Done')
            return ScanResult(0, None, work_files)

        if archive:
            work_files = [archive_scans(workdir, '*.{}'.format(exten), archive)]

        target = '{}/{}.{}'.format(SCAN_DIR, output_name, exten if not archive else archive_exten(archive))
        rename(join(workdir, work_files[0]), target)
        logging.info('Done: {}'.format(target))
        return ScanResult(0, None, [target])

    except Exception as e:
        logging.exception('Scan error: {}'.format(e))

    finally:
        logging.info('Cleanup')
        rmtree(workdir)


def batch(result_format, result_name, files):
    """
    Batch many files to one (converting)
    :param result_format: Format (tif, pdf)
    :param result_name: Result filename
    :param files: Files object list
    :return:
    """
    workdir = join(SCAN_DIR, 'temp', 'tmpconvert-{}'.format(now()))
    try:
        Path(workdir).mkdir(parents=True, exist_ok=True)
        i = 0
        spacer_len = len(str(len(files))) + 1
        convert_list = list()
        for file in files:
            i += 1
            parts = file.name.split('.')
            exten = parts[-1].lower() if len(parts) > 1 else parts.lower()
            filename = join(workdir, 'out' + zero_fill(str(i), spacer_len) + '.' + result_format)
            convert_list.append(filename)

            if exten == 'tif':
                if result_format == 'tif':
                    logging.debug("Copy {} to {}".format(file.name, filename))
                    copyfile(file.name, filename)
                else:
                    logging.debug("tif2pdf {} to {}".format(file.name, filename))
                    tiff2pdf(file.name, filename, 100)
            elif exten == 'pdf':
                if result_format == 'pdf':
                    logging.debug("Copy {} to {}".format(file.name, filename))
                    copyfile(file.name, filename)
                else:
                    logging.debug("pdf2tif {} to {}".format(file.name, filename))
                    pdf2tiff(file.name, filename, 288)
                    # exec_bash('gs -q -r300 -dNOPAUSE -sDEVICE=tiffg4 -sOutputFile="{out}" "{inp}" -c quit'.format(
                    #     inp=file.name,
                    #     out=filename
                    # ))
            elif exten in ['jpg', 'png']:
                logging.debug("convert {} to {}".format(file.name, filename))
                exec_bash('convert -quality 100 "{inp}" "{out}"'.format(
                    inp=file.name,
                    out=filename
                ))

        if result_format == 'tif':
            logging.debug("Batch")
            file = batch_to_single(workdir)
        else:
            file = join(workdir, 'result.pdf')
            logging.debug("Merge ({})".format(file))
            result = exec_bash('gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dAutoRotatePages=/None \
            -sOutputFile="{out}" {inps}'.format(
                out=file,
                inps=" ".join('"{}"'.format(x) for x in convert_list)
            ))

        target = '{}/{}.{}'.format(SCAN_DIR, result_name, result_format)
        rename(file, target)
        logging.info('Done: {}'.format(target))
    finally:
        logging.info('Cleanup')
        # rmtree(workdir)


def set_test_mode(set_to):
    global TEST_MODE
    TEST_MODE = set_to


@click.group()
@click.option('--debug', is_flag=True)
@click.option('--test', is_flag=True)
def cli(debug, test):
    """Scanner universal API (via scanimage)"""
    global TEST_MODE
    if debug:
        logging.getLogger('').setLevel(logging.DEBUG)
        logging.debug('Debug is enabled')
    if test:
        TEST_MODE = True


@cli.command('scan', context_settings={"ignore_unknown_options": True})
@click.argument('device_id')
@click.argument('output_name')
@click.option('--source', '-s', default='Flatbed', type=click.Choice(['Flatbed', 'ADF', '']),
              help='Papper source. Flatbet or Automatic Document Feeder.\nDefault: Flatbed.', metavar='<Flatbed|ADF>')
@click.option('--pages', '-n', default=1, help='Page count\nDefault: 1', metavar='<N>')
@click.option('--archive', '-z', type=click.Choice(ALLOWED_ARCHIVE_FORMATS), default='',
              help='Archivate result with zip or tar+gzip format', metavar='<zip|tar>')
@click.option('--dpi', '-r', default=200, help='Resolution (DPI)\nDefault: 200', metavar='<int>')
@click.option('--color', '-c', is_flag=True, help='Use color scan (instead of black-white)')
@click.option('--exten', '-e', type=click.Choice(ALLOWED_SCAN_FORMATS), default='pdf',
              help='Result format (TIFF, PDF, JPEG)\nDefault: pdf')
@click.option('--quality', '-q', default=80, help='Quality of image (only JPG, PNG and PDF)\nDefault: 80',
              metavar='<percents>')
def cli_scan_images(device_id, output_name, source, pages, archive, dpi, color, exten, quality):
    scan_images(device_id, output_name, source, pages, archive, dpi, color, exten, quality)


@cli.command('devices', short_help='Show possible device list')
@click.option('--update', '-u', is_flag=True, help='Update device list')
def device_list(update):
    devices = get_device_list(force_update=update)
    if not devices:
        logging.error('Devices not found. {}'.format('' if update else 'Use --update option to update list'))
        return

    print('--------------------------------------------------------------')
    print(COLUMS_FORMAT.format('Device ID', 'Device name'))
    print('--------------------------------------------------------------')
    for dev in devices:
        print(COLUMS_FORMAT.format(dev.device_id, dev.name))


@cli.command('batch', short_help='Converting files')
@click.option('--format', '-f', 'format_', type=click.Choice(ALLOWED_BATCH_FORMATS), default='tif',
              help='Output format PDF or TIFF')
@click.argument('result_name')
@click.argument('files', nargs=-1, type=click.File('r'))
def cli_batch(format_, result_name, files):
    batch(format_, result_name, files)


if __name__ == '__main__':
    cli()
