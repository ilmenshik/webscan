# Webscan
Web scanner interface for devices that does not support networking

# Requrements

## System
 * Ubuntu 18+
 * python3.5+ (+requirements.txt)
 * sane-utils
 * libtiff-tools
 * imagemagick
 * zip
 * xmlstarlet (only for install.sh)

## Other requirements
* File: `/etc/ImageMagick-6/policy.xml`
    * Edit line: `<policy domain="coder" rights="read|write" pattern="PDF" />`
    * Add line: `<policy domain="coder" rights="read|write" pattern="LABEL" />`
* File: `/etc/sudoers` (visudo)
    * Add line `www-data ALL=(ALL:ALL) NOPASSWD: /usr/bin/scanimage`  

# Install & launch
## Automagicaly:
* Recommend path: /opt/webscan
* Default username: www-data
```
bash install.sh install [service_username]
```

## Manually:
System requirements description:
- sane-utils: scanimage - scanning software
- libtiff-tools: tiff2pdf converter, tiffcp tool
- imagemagick: image convert tool
- zip: archivator
- xmlstarlet (only for install.sh): xml editor, for edit imagemagick policy 

```bash
apt install -y python3-pip sane-utils libtiff-tools imagemagick zip xmlstarlet
```

Python:
```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python app.py
```

Custom settings:
```bash
export WEBSCAN_PORT=5050
export WEBSCAN_LISTEN=127.0.0.1
```

Work url: http://127.0.0.1:5050

# CLI
Update scanner device list with test data:
```bash
python ctrl/scan.py --test devices -u
```
Adding parameter "--test" also will scan test image instead real work.
