import logging
from os import environ
from os.path import basename, join
from flask import Flask, render_template, Response, abort, send_file, request, send_from_directory, url_for
from datetime import datetime
from json import dumps
from ctrl.methods import (
    get_scan_list,
    get_scan_object,
    plurals,
    filter_result_filename,
    SCANNS_DIR
)
from ctrl.scan import (
    DEVICE_LIST,
    get_device_list,
    batch,
    scan_images,
    archive_scans, now,
    ALLOWED_ARCHIVE_FORMATS,
    ALLOWED_SCAN_FORMATS,
    set_test_mode,
)
from os import remove
from sys import argv


app = Flask(__name__)
SCANNER_IS_BUSY = False


@app.errorhandler(403)
def error_page_403(e):
    logging.error(e)
    return render_template('403.html'), 403


@app.errorhandler(404)
@app.errorhandler(500)
def custom_error_page(e=None):
    return render_template('_error.html', **{
        'error_code': e.code,
        'error_message': e.description}
    ), e.code


@app.route('/403', methods=['GET', 'POST'], strict_slashes=False)
@app.route('/404', methods=['GET', 'POST'], strict_slashes=False)
@app.route('/500', methods=['GET', 'POST'], strict_slashes=False)
def test_error_page():
    error_code = int(str(request.path)[1:])
    return abort(error_code)


def int_in_range_or_default(val, default, min_limit=None, max_limit=None):
    try:
        i = int(val)
        if min_limit is None and max_limit is None:
            return i
        if min_limit is None and max_limit is not None:
            return i if i <= max_limit else default
        if min_limit is not None and max_limit is None:
            return i if i >= min_limit else default

        return i if i in range(min_limit or 0, max_limit + 1) else default
    except Exception as e:
        logging.debug(e)
    return default


@app.route('/api/<action>', methods=['GET', 'POST'], strict_slashes=False)
def rest_api(action):
    debug = None
    success = False
    data = None
    message = ''
    form = request.form  # short alias
    global SCANNER_IS_BUSY
    try:
        if action == 'scan':
            SCANNER_IS_BUSY = True
            try:
                scan_device = form.get('scanDevice')
                if not scan_device:
                    message = 'Не указано сканирующее устройство'

                if not DEVICE_LIST.device_exist(scan_device):
                    message = 'Устройство не найдено'

                scan_filename = filter_result_filename(form.get('scanFilename')) or 'scan-{}'.format(now())
                scan_source = 'Flatbed' if form.get('scanSource') == 'Flatbed' else 'ADF'
                scan_pages = 1
                if scan_source == 'ADF' and form.get('scanPages') == '99':
                    scan_pages = int_in_range_or_default(form.get('scanPagesCount'), 1, 1)

                result = scan_images(
                    device_id=scan_device,
                    output_name=scan_filename,
                    source=scan_source,
                    pages=scan_pages,
                    archive=form.get('scanArchive') if form.get('scanArchive') in ALLOWED_ARCHIVE_FORMATS else '',
                    dpi=int_in_range_or_default(form.get('scanDPI'), 200, 100, 6000),
                    color=form.get('scanColor') == 'Color',
                    exten=form.get('scanExtension') if form.get('scanExtension') in ALLOWED_SCAN_FORMATS else 'pdf',
                    quality=int_in_range_or_default(form.get('scanImageQuality'), 80, 10, 100))

                if result.exit_code == 0:
                    success = True
                    row_html = None
                    if len(result.result_files) == 1:
                        scan = get_scan_object(result.result_files[0])
                        row_html = '''
                        <tr>
                            <td>
                                {date}
                            </td>
                            <td class="scanItem">
                                <a href="/download/{name}">
                                    <img src="{url}" class="scanIcon" />
                                    <div class="scanFile">{name}</div>
                                </a>
                            </td>
                            <td class="scanSize">
                                {size}
                            </td>
                            <td>
                                <a href="#" class="scanRemove" data="{name}">Удалить</a>
                            </td>
                        </tr>
                        '''.format(
                            date=scan.datetime,
                            name=scan.name,
                            url=url_for('static', filename='css/icons/{}.png'.format(scan.exten)),
                            size=scan.size
                        )

                    data = {
                        'row': row_html,
                        'result_name': basename(result.result_files[0]) if len(result.result_files) == 1 else None
                    }
                else:
                    message = result.error_log
            except Exception as e:
                logging.exception(e)
            finally:
                SCANNER_IS_BUSY = False

        elif action == 'devices':

            devices = get_device_list(force_update=True)
            if devices:
                success = True
                message = 'Найдено {} устройств{}'.format(devices, plurals(devices, 'о', 'а', ''))
            else:
                message = 'Устройства не найдены'

        elif action == 'convert':
            files = list()
            for filename in form.getlist('files[]'):
                file = get_scan_object(filename)
                if file:
                    files.append(open(file.path))
            debug = batch(
                'pdf' if form.get('file_type') == 'pdf' else 'tif',
                form.get('file_name'),
                sorted(files, key=lambda file: file.name)
            )
            success = True

        elif action == 'archive':

            file_list_escaped_str = ''
            for filename in form.getlist('files[]'):
                file = get_scan_object(filename)
                if file:
                    file_list_escaped_str += ' "{}"'.format(file.name)

            filename = filter_result_filename(form.get('file_name'))
            if file_list_escaped_str and filename:
                data = {'result_name': archive_scans(SCANNS_DIR, file_list_escaped_str, 'zip', filename)}

            success = True

        elif action == 'delete':

            i = 0
            for filename in form.getlist('files[]'):
                file = get_scan_object(filename)
                if file:
                    i += 1
                    remove(file.path)
            message = "Удален{} {} Файл{}".format(
                plurals(i, '', 'ы', 'о'),
                i, plurals(i, '', 'а', 'ов'))
            success = True

        else:

            raise Exception('Неизвестное действие "{}"'.format(action))

    except Exception as e:
        from traceback import format_exc
        debug = "{}: {}\n{}".format(action, e, format_exc())

    return Response(dumps({
        'success': success,
        'message': message,
        'data': data,
        'debug': debug
    }), mimetype='application/json')


@app.route('/scans', methods=['GET'], strict_slashes=False)
def scans_list():
    ctx = {
        'scans_list': get_scan_list(),
        'scans_mass_actions': True
    }
    return render_template('scans.html', **ctx)


@app.route('/download/<filename>', methods=['GET'], strict_slashes=False)
def scan_download(filename):
    file = get_scan_object(filename)
    if not file:
        abort(404, description='Файл "{}" не найден'.format(filename))
    return send_file(file.path, attachment_filename=file.name)


@app.route('/', methods=['GET'], strict_slashes=False)
def index():
    scans = get_scan_list()
    ctx = {
        'scanner_list': get_device_list(),
        'new_scan_name': 'scan-{}'.format(datetime.now().strftime('%f')[-6:]),
        'scans_list': scans[:10] if scans else None,
        'scans_mass_actions': False,
        'scanner_is_busy': SCANNER_IS_BUSY
    }
    return render_template('index.html', **ctx)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(
        join(app.root_path, 'static'),
        'favicon.ico',
        mimetype='image/x-icon'
    )


if __name__ == '__main__':
    logging.getLogger('').setLevel(logging.DEBUG if '--debug' in argv else logging.INFO)
    set_test_mode('--test' in argv)
    app.run(
        host=environ.get('WEBSCAN_LISTEN', '127.0.0.1'),
        port=environ.get('WEBSCAN_PORT', 5050),
        threaded=True,
        debug='--debug' in argv
    )
