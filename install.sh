#!/bin/bash
set -e
export WEBSCAN_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
export WEBSCAN_USER="${2:-www-data}"
export MODE="$1"

if [[ ! "$MODE" =~ ^(un)?install$ ]]; then
  echo "Use: $0 <install|uninstall> [service_user]"
  exit 1
fi

INSTALL_PACKAGES="python3-pip sane-utils libtiff-tools imagemagick zip xmlstarlet"
IMAGEMAGICK_POLICY_FILE="/etc/ImageMagick-6/policy.xml"
SUDOERS_SCANIMAGE="$WEBSCAN_USER ALL=(ALL:ALL) NOPASSWD: /usr/bin/scanimage"
CRONTAB_ROTATE_SCANS="00 00   * * *  $WEBSCAN_USER /bin/bash '$WEBSCAN_DIR/ctrl/rotate_scans.sh' 30"

function banner() {
  echo
  echo "--- [ $1 ] ----------------------"
  echo
}

function rerun_as_root() {
  if [[ "$(id -u)" != "0" ]]; then
    echo 'Need to be root...'
    sudo "$0" "$MODE" "$WEBSCAN_USER"
    exit
  fi
}

function create_service_user() {
  if ! grep -P "^$WEBSCAN_USER:" /etc/passwd > /dev/null; then
    banner "Creating service user: $WEBSCAN_USER"
    sudo adduser \
      --shell /usr/sbin/nologin \
      --home "$WEBSCAN_DIR" \
      --disabled-password \
      --no-create-home \
      --gecos "" \
      "$WEBSCAN_USER"
  fi
  echo "OK"
}

function install_system_packages() {
  banner "Install system packages"
  for pak in $INSTALL_PACKAGES; do
    t=$(dpkg -l | grep "$pak" | head -n 1)
    echo "Checking $pak"
    if [[ "$t" == '' ]]; then
      apt-get install -y "$pak"
    fi
  done
  echo "OK"
}

function update_imagemagic_policy() {
  banner "Updating imagemagic policy for convert..."

  backup="${IMAGEMAGICK_POLICY_FILE}_backup$(date +%F_%H%M%S)"
  if [[ ! -f "$backup" ]]; then
    cp -v "$IMAGEMAGICK_POLICY_FILE" "$backup"
  fi

  for pattern in PDF LABEL; do
    if xmlstarlet sel -t -c "/policymap/policy[@pattern='${pattern}']" "$IMAGEMAGICK_POLICY_FILE" >/dev/null; then
      echo "Pattern ${pattern} found, updating"
      xmlstarlet edit --inplace -u "/policymap/policy[@pattern='${pattern}']/@rights" -v "read|write" "$IMAGEMAGICK_POLICY_FILE"
    else
      echo "Pattern ${pattern} not found, inserting"
      xmlstarlet edit --inplace -s /policymap -t elem -n "policy xmlns=\"\" domain=\"coder\" rights=\"read|write\" pattern=\"${pattern}\"" "$IMAGEMAGICK_POLICY_FILE"
    fi
  done
  echo "OK"
}

function allow_user_scanimage() {
  banner "Permit user $WEBSCAN_USER 'sudo scanimage' without password..."
  test=$(grep scanimage /etc/sudoers | grep "$WEBSCAN_USER" | grep NOPASSWD || true)
  if [[ "$test" == '' ]]; then
    echo "$SUDOERS_SCANIMAGE" >> /etc/sudoers
  fi
  echo "OK"
}

function update_cron_task() {
  test=$(grep "ctrl/rotate_scans.sh" /etc/crontab | grep "$WEBSCAN_USER" || true)
  if [[ "$test" == '' ]]; then
    banner "Install cronjob for (remove old scans)"
    cat << EOF >> /etc/crontab
#
# Removing old scans
#
$CRONTAB_ROTATE_SCANS
EOF
  else
    banner "Update cronjob (remove old scans)"
    sed -i "s|^.*ctrl/rotate_scans.sh.*$|${CRONTAB_ROTATE_SCANS}|" /etc/crontab
  fi
  echo "OK"
}

function prepare_venv() {
  banner 'Prepearing venv...'
  cd "$WEBSCAN_DIR" || exit
  if [[ ! -d venv ]]; then
    python3 -m venv venv
  fi
  source venv/bin/activate
  banner 'Install requirements...'
  pip3 install -r requirements.txt
  echo "OK"
}

function update_devices() {
  banner 'Updating scanners...'
  python3 ctrl/scan.py devices --update
  echo "OK"
}

function fix_files_owner() {
  banner "Fixing directory owner"
  if [[ ! "$WEBSCAN_DIR" =~ ^/opt/ ]]; then
    {
      echo "ERROR: Cannot chown directory '$WEBSCAN_DIR' !"
      echo "May be trouble with rights!"
      echo "Run 'chown -R $WEBSCAN_USER:$WEBSCAN_USER "$WEBSCAN_DIR"' manually at one's own risk"
    } | grep --color=always '.*'
  else
    chown -R $WEBSCAN_USER:$WEBSCAN_USER "$WEBSCAN_DIR"
    echo "OK"
  fi
}

function install_service() {
  banner "Installing service"
  envsubst < webscan.service > /lib/systemd/system/webscan.service
  systemctl daemon-reload
  systemctl enable webscan
  systemctl start webscan
  sleep 2
  systemctl status webscan
  echo "OK"
}

function uninstall() {
  banner "Removing $WEBSCAN_USER from sudoers"
  grep -v "$SUDOERS_SCANIMAGE" /etc/sudoers > /tmp/sudoers
  cat /tmp/sudoers > /etc/sudoers

  banner "Removing crontab job"
  grep -v "ctrl/rotate_scans.sh" /etc/crontab > /tmp/crontab
  cat /tmp/crontab > /etc/crontab

  banner "Removing venv"
  rm -rf venv

  banner "Removing service"
  systemctl stop webscan || true
  systemctl disable webscan || true
  rm -v /lib/systemd/system/webscan.service || true
  systemctl daemon-reload
}

rerun_as_root

if [[ "$MODE" == 'install' ]]; then
  create_service_user
  install_system_packages
  update_imagemagic_policy
  allow_user_scanimage
  update_cron_task
  prepare_venv
  update_devices
  fix_files_owner
  install_service
elif [[ $MODE == 'uninstall' ]]; then
  uninstall
fi

banner "Success ${MODE}ation"
